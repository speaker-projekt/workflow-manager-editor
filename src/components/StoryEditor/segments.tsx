import {INode, REACT_FLOW_CHART} from "@mrblenny/react-flow-chart/src";
import * as React from "react";
import {Button, Tag} from "@blueprintjs/core";
import {SelectOnChartCallback} from "./ChartComponents/SelectedNode";

export interface ISegment {
    id: string;
    content: string;
    documentId: string;
}

export interface ISegmentRecommendation {
    segment: ISegment;
    selectedDocument: boolean;
    relation?: string;
    selectOnChart: SelectOnChartCallback;
}

export const SegmentRecommendation = ({ segment, selectedDocument, relation, selectOnChart }: ISegmentRecommendation) => {

    const type = 'segment';
    const ports: INode['ports'] = {
        port1: {
            id: 'port1',
            type: 'top',
            properties: {
                custom: 'property',
            },
        },
        port2: {
            id: 'port1',
            type: 'bottom',
            properties: {
                custom: 'property',
            },
        },
    };

    return (
        <div className={selectedDocument ? "recommendation-segment recommendation-selected" : "recommendation-segment"}>
            <div onClick={() => {
                selectOnChart({
                    type: 'node',
                    id: segment.id,
                });
            }}>
                <p>
                    <Button
                        small={true}
                        icon="move"
                        draggable={true}
                        onDragStart={ (event) => {
                            event.dataTransfer.setData(REACT_FLOW_CHART, JSON.stringify({ type, ports, properties: Object.assign({label: relation}, segment) }))
                        } }
                    />&nbsp;
                    {relation && (<Tag>{relation}</Tag>)} {segment.content}</p>
                {/*<Button*/}
                {/*    text="Add"*/}
                {/*    onClick={(event: any) => {*/}
                {/*        event.dataTransfer.setData(REACT_FLOW_CHART, JSON.stringify({ type, ports, properties }))*/}
                {/*    }}*/}
                {/*/>*/}
            </div>
        </div>
    )
};
