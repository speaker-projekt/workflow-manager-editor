import React, { useState } from "react";
import {Button, Tag} from "@blueprintjs/core";
import {DOCUMENTS, SEGMENTS} from "../misc/data";
import {DocumentRecommendation, IDocument} from "../documents";
import {ISegment, SegmentRecommendation} from "../segments";
import {ISelectedNode} from "./SelectedNode";

function seedShuffle(array: any[], seed: number) {
    let currentIndex = array.length, temporaryValue, randomIndex;
    seed = seed || 1;
    let random = function() {
        var x = Math.sin(seed++) * 10000;
        return x - Math.floor(x);
    };
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(random() * currentIndex);
        currentIndex -= 1;
        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
    return array;
}

function shuffle(array: any[], seed: number) {                // <-- ADDED ARGUMENT
    var m = array.length, t, i;

    // While there remain elements to shuffle…
    while (m) {

        // Pick a remaining element…
        i = Math.floor(random(seed) * m--);        // <-- MODIFIED LINE

        // And swap it with the current element.
        t = array[m];
        array[m] = array[i];
        array[i] = t;
        ++seed                                     // <-- ADDED LINE
    }

    return array;
}

function random(seed: number) {
    var x = Math.sin(seed++) * 10000;
    return x - Math.floor(x);
}

function text2binary(s: string) {
    return s.split('').map(function (char) {
        return char.charCodeAt(0);
    }).reduce(function(a, b){
        return a + b;
    }, 0);
}

export function getRelation(seedId: string, segment: ISegment) {
    const relationId: string = seedId + segment.id;
    let rels = [
        "BACKGROUND",
        "EXPANSION",
        "CONTIGENCY",
        "COMPARISON",
    ];
    const randomRelations = seedShuffle(rels, text2binary(relationId));

    return randomRelations[1];
    // return String(text2binary(relationId));
}

export const SelectedSegment = ({ nodeId, stateActions, handleChartChange, selectOnChart, node }: ISelectedNode) => {
    console.log(handleChartChange);

    const {content} = node.properties;
    let documents: IDocument[] = [];
    let segments: ISegment[] = [];

    const [selectedDocumentId, setSelectedDocumentId] = useState('');

    if(nodeId === undefined) {
        return <></>;
    }

    documents = shuffle(Object.values(DOCUMENTS), content.length);
    segments = shuffle(Object.values(SEGMENTS), content.length);

    const segment = SEGMENTS[nodeId];
    let screenshot = 'default.png';

    if(segment && DOCUMENTS.hasOwnProperty(segment.documentId)) {
        const document = DOCUMENTS[segment.documentId];

        screenshot = document.screenshot ? document.screenshot : 'default.png';
    }



    return (
        <div className="selected-node-wrapper">
            <div className="selected-node" data-id={nodeId}>
                <div>
                    <Tag>{node.type.toUpperCase()}</Tag>
                    <p><br />{content}</p>
                    <p>
                        <small>Source:</small>
                        <br/>
                        <img src={`${process.env.PUBLIC_URL}/static/screenshots/${screenshot}`} alt="screenshot" />
                    </p>
                </div>
                <Button
                    intent="primary"
                    icon="document-open"
                    text="View source"
                    />
                <span> </span>
                <span> </span>
                <Button
                    icon="delete"
                    intent="danger"
                    text="Delete"
                    onClick={() => {
                        const config = { readonly: false };
                        stateActions.onDeleteKey({config})
                    }}/>
            </div>
            <div className="recommendations-wrapper">

                <div className="recommendations">
                    <div className="recommendations-header">
                        Similar Segments
                    </div>
                    <div className="recommendations-body">
                        {segments.length > 0 ? segments.map((segment: ISegment) => {
                            return (
                                <SegmentRecommendation
                                    segment={segment}
                                    relation={getRelation(nodeId, segment)}
                                    selectedDocument={selectedDocumentId === segment.documentId}
                                    selectOnChart={selectOnChart}
                                />
                            )
                        }) : (
                            <i>No segments available.</i>
                        )}
                    </div>
                    <div className="recommendations-footer">
                    </div>
                </div>
                <div className="recommendations">
                    <div className="recommendations-header">
                        Documents
                    </div>
                    <div className="recommendations-body">
                        {documents.length > 0 ? documents.map((document: IDocument) => {
                            return (
                                <DocumentRecommendation
                                    document={document}
                                    selected={selectedDocumentId === document.id}
                                    handleOnClick={() => {setSelectedDocumentId(document.id)}}
                                    selectOnChart={selectOnChart}
                                />
                            )
                        }) : (
                            <i>No documents available.</i>
                        ) }
                    </div>
                    <div className="recommendations-footer">
                    </div>
                </div>
            </div>

        </div>
    )
};
