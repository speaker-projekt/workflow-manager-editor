import React, { useState } from "react";
import {Button, Tag} from "@blueprintjs/core";
import {getDocumentsByTopicId, getSegmentsByTopicId, STORIES} from "../misc/data";
import {DocumentRecommendation, IDocument} from "../documents";
import {ISegment, SegmentRecommendation} from "../segments";
import {cloneDeep} from "lodash";
import {getChartFromStory} from "../stories";
import {ISelectedNode} from "./SelectedNode";
import {getRelation} from "./SelectedSegment";


export const SelectedTopic = ({ nodeId, stateActions, handleChartChange, selectOnChart, node }: ISelectedNode) => {

    const {title, subtitle, description} = node.properties;
    let documents: IDocument[] = [];
    let segments: ISegment[] = [];

    const [selectedDocumentId, setSelectedDocumentId] = useState('');

    if(!nodeId) {
        return <></>;
    }

    documents = getDocumentsByTopicId(nodeId);
    segments = getSegmentsByTopicId(nodeId);

    return (
        <div className="selected-node-wrapper">
            <div className="selected-node" data-id={nodeId}>
                <div>
                    <Tag>{node.type.toUpperCase()}</Tag>
                    <h2>{title}</h2>
                    <h3>{subtitle}</h3>
                    <p>{description}</p>
                </div>
                <Button
                    intent="success"
                    icon="automatic-updates"
                    text="Generate story"
                    onClick={() => {
                        handleChartChange(cloneDeep(getChartFromStory(STORIES[0])));
                    }}
                />
                <span> </span>
                <Button
                    icon="delete"
                    intent="danger"
                    text="Delete"
                    onClick={() => {
                        const config = { readonly: false };
                        stateActions.onDeleteKey({config})
                    }}/>
            </div>
            <div className="recommendations-wrapper">

                <div className="recommendations">
                    <div className="recommendations-header">
                        Text Segments
                    </div>
                    <div className="recommendations-body">
                        {segments.length > 0 ? segments.map((segment: ISegment) => {
                            return (
                                <SegmentRecommendation
                                    segment={segment}
                                    relation={getRelation(nodeId, segment)}
                                    selectedDocument={selectedDocumentId === segment.documentId}
                                    selectOnChart={selectOnChart}
                                />
                            )
                        }) : (
                            <i>No segments available.</i>
                        )}
                    </div>
                    <div className="recommendations-footer">
                    </div>
                </div>
                <div className="recommendations">
                    <div className="recommendations-header">
                        Documents
                    </div>
                    <div className="recommendations-body">
                        {documents.length > 0 ? documents.map((document: IDocument) => {
                            return (
                                <DocumentRecommendation
                                    document={document}
                                    selected={selectedDocumentId === document.id}
                                    handleOnClick={() => {setSelectedDocumentId(document.id)}}
                                    selectOnChart={selectOnChart}
                                />
                            )
                        }) : (
                            <i>No documents available.</i>
                        ) }
                    </div>
                    <div className="recommendations-footer">
                    </div>
                </div>
            </div>

        </div>
    )
};
