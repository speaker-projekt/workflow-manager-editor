import * as React from "react";
import {INodeDefaultProps} from "@mrblenny/react-flow-chart/src";
import styled from "styled-components";

const DocumentBox = styled.div`
  position: absolute;
  width: 300px;
  height: 150px;
  padding: 5px;
  background: #3e3e3e;
  color: white;
  border-radius: 10px;
`;

const TopicBox = styled.div`
  position: absolute;
  width: 300px;
  height: 100px;
  padding: 10px;
  padding-top: 25px;
  display: flex;
  justify-content: center;
  align-items: center;
  background: cornflowerblue;
  color: white;
  text-align: center;
  border-radius: 10px;
`;

const SegmentBox = styled.div`
  position: absolute;
  width: 300px;
  height: 100px;
  padding: 5px;
  background: #fff;
  color: #000;
  border-color: #000;
  border-radius: 10px;
`;

/**
 * Create the custom component,
 * Make sure it has the same prop signature
 * You'll need to add {...otherProps} so the event listeners are added to your component
 */
export const NodeCustom = React.forwardRef(({ node, children, ...otherProps }: INodeDefaultProps, ref: React.Ref<HTMLDivElement>) => {
    if (node.type === 'topic') {
        return (
            <TopicBox ref={ref} {...otherProps}>
                {children}
            </TopicBox>
        )
    } else if (node.type === 'segment') {
        return (
            <SegmentBox ref={ref} {...otherProps}>
                {children}
            </SegmentBox>
        )
    } else {
        return (
            <DocumentBox ref={ref} {...otherProps}>
                {children}
            </DocumentBox>
        )
    }
});

// export const NodeCustom = React.forwardRef(({ node, children, ...otherProps }: INodeDefaultProps, ref: React.Ref<HTMLDivElement>) => {
//     const nodeClass = node.type === 'topic' ? 'node-wrapper-topic' : 'node-wrapper-document';
//
//     otherProps.className = classNames("docs-modifiers", this.props.className)
//
//     return (
//         <div className={nodeClass} ref={ref} {...otherProps}>
//             {children}
//         </div>
//     );
// });
