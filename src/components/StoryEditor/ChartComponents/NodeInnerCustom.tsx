import {INodeInnerDefaultProps} from "@mrblenny/react-flow-chart/src";
import * as React from "react";
import styled from "styled-components";
import {Tag} from "@blueprintjs/core";

const Outer = styled.div`
  padding: 10px;
  height: 140px;
  overflow: hidden;
`;

const TopicOuter = styled.div`
  padding: 10px;
  height: 90px;
  overflow: hidden;
`;

const SegmentOuter = styled.div`
  padding: 10px;
  height: 90px;
  overflow: hidden;
`;

/**
 * Create the custom component,
 * Make sure it has the same prop signature
 */
export const NodeInnerCustom = ({ node, config }: INodeInnerDefaultProps) => {
    if (node.type === 'topic') {
        return (
            <TopicOuter className="node node-topic">
                {node.properties && (
                    <>
                        {/*<Tag>Topic</Tag>*/}
                        <h2>{node.properties.title}</h2>
                        <p>{node.properties.description}</p>
                    </>
                )}
            </TopicOuter>
        )
    } else if (node.type === 'segment') {
        return (
            <SegmentOuter className={node.properties && node.properties.relevance && node.properties.relevance < 1 ? "node node-segment not-relevant" : "node node-segment"}>
                {node.properties ? (
                    <>
                        <p><Tag>{node.properties.rank}</Tag> {node.properties.content}</p>
                    </>
                ) : (
                    <p><i>No text content available</i></p>
                )}
                <div className="segment-source">Wikinews: Moabit</div>
            </SegmentOuter>
        );
    } else {
        return (
            <Outer className="node node-document">
                <Tag>Document</Tag>
                {node.properties && (
                    <>
                        <h2>{node.properties.title}</h2>
                        <h3>{node.properties.subtitle}</h3>
                        <p>{node.properties.description}</p>

                    </>
                )}
                <br />
                <input
                    type="checkbox"
                    name=""
                    value="1"
                    onChange={(e) => console.log(e)}
                    onClick={(e) => e.stopPropagation()}
                    onMouseUp={(e) => e.stopPropagation()}
                    onMouseDown={(e) => e.stopPropagation()}
                />
            </Outer>
        )
    }
};
