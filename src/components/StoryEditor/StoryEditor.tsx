import React from "react";
import {StoryChart} from "./StoryChart";
import {cloneDeep} from "lodash";

import './StoryEditor.scss';
import {STORIES} from "./misc/data";
import {getChartFromStory} from "./stories";


class StoryEditor extends React.Component<{}, { chart: any}> {
    constructor(props: any) {
        super(props);

        this.state = {
            chart: cloneDeep(getChartFromStory(STORIES[0])),
        };

    }

    render() {
        return (
            <div className="story-wrapper">
                <div style={{height: '500px'}}>
                    {/*<CustomNodeDemo />*/}
                    {/*<DragAndDropSidebar />*/}
                    <StoryChart chart={this.state.chart} />
                </div>
            </div>
        );
    }
}


export default StoryEditor;
