import {IPosition} from "@mrblenny/react-flow-chart/src";
import * as React from "react";
import {Icon} from "@blueprintjs/core";
import {SelectOnChartCallback} from "./ChartComponents/SelectedNode";

export interface IDocument {
    id: string;
    title: string;
    subtitle?: string;
    topicId: string;
    description?: string;
    source?: string;
    screenshot?: string;
    position?: IPosition;
}

export interface IDocumentRecommendation {
    document: IDocument;
    selected: boolean;
    handleOnClick: any;
    selectOnChart: SelectOnChartCallback;
}

export const DocumentRecommendation = ({ document, selected, handleOnClick, selectOnChart }: IDocumentRecommendation) => {

    // const type = 'document';
    // const ports: INode['ports'] = {
    //     port1: {
    //         id: 'port1',
    //         type: 'top',
    //         properties: {
    //             custom: 'property',
    //         },
    //     },
    //     port2: {
    //         id: 'port1',
    //         type: 'bottom',
    //         properties: {
    //             custom: 'property',
    //         },
    //     },
    // };

    return (
        <div className={selected ? "recommendation-document recommendation-selected" : "recommendation-document"}
             // draggable={true}
             // onDragStart={ (event) => {
             //     event.dataTransfer.setData(REACT_FLOW_CHART, JSON.stringify({ type, ports, properties: Object.assign({}, document) }))
             // } }
             // onClick={() => {
             //         selectOnChart({
             //         type: 'node',
             //         id: document.id,
             //     });
             // }}
             onClick={handleOnClick}
             onMouseOver={handleOnClick}
        >
            <div>
                <h2><Icon icon="document" />{document.title}</h2>
                <h3>{document.subtitle}</h3>
            </div>
        </div>
    )
};
