import {ITopic} from "../topics";
import {IDocument} from "../documents";
import {ISegment} from "../segments";
import {IStory} from "../stories";

// Dummy relations
export const RELATIONS: string[] = [
    "BACKGROUND",
    "EXPANSION",
    "CONTIGENCY",
    "COMPARISON",
];

// Available topics
export const TOPICS: ITopic[] = [
    { id: "moabit", title: "Moabit", description: "Stadtteil von Berlin"},
    { id: "covid", title: "COVID-19", description: "Die Pandemie von Welt"},
    { id: "ai", title: "Artificial Intelligence", description: "Stadtteil von Berlin"},
    { id: "usa", title: "US Wahlkampf 2020", description: "Stadtteil von Berlin"},
    { id: "climate", title: "Klimawandel", description: "Stadtteil von Berlin"},
    { id: "moabit-aeg", title: "AEG Turbine Factory", description: "Turbine Factory in Moabit, best-known work of architect Peter Behrens from around 1909"},
].map((m, index) => ({ ...m, rank: index + 1 }));

// Available documents
export const DOCUMENTS: {[id: string]: IDocument; } = {};

DOCUMENTS["west-harbour"] = {
    id: "west-harbour",
    title: "West Harbour",
    subtitle: "Initial planning for the construction of a large port in Berlin started ",
    description: "West Harbour is a inland harbor in Berlin's Moabit district.",
    topicId: "moabit", // topic id must match with id in topics
    source: "Wikinews",
};

DOCUMENTS["deutsche-einheit"] = {
        id: "deutsche-einheit",
        topicId: "moabit",
        title: 'Deutsche Einheit',
        subtitle: 'Verkehrsprojekt Deutsche Einheit Nr. 17',
        description: 'Der Westhafen ist Bestandteil des aus ökologischen Gründen umstrittenen Verkehrsprojekts Deutsche Einheit Nr. 17. Mit dem Ausbau der Wasserstraßenverbindung Hannover–Magdeburg–Berlin können Binnenschiffe mit bis zu 2000 Tonnen und mit einer Abladetiefe von bis zu 2,80 Meter den Westhafen erreichen. ',
        source: 'Wikinews'
};

DOCUMENTS["bsk"] = {
        id: "bsk",
        title: 'Binnenschifferkirche',
        subtitle: 'Von 1968 bis 2009 befand',
        description: 'sich an der Westhafenstraße 1 eine Schiffer- und Hafenkirche in einem ehemaligen Ladengebäude. Sie ist Nachfolgerin einer von der Schiffergemeinde bis 1943 genutzten schwimmenden Kirche.',
        source: 'Wikinews',
        topicId: "moabit",
    screenshot: "spon.jpg"
};

DOCUMENTS["aeg-4"] = {
    id: "aeg-4",
    title: 'Moabit AEG Turbine Factory',
    description: 'The AEG turbine factory was built around 1909, at Huttenstraße 12-16 in the Berlin district of Moabit. It is the best-known work of architect Peter Behrens.[1] The 100m long steel framed building with 15m tall glass windows on either side is considered the first attempt to introduce restrained modern design to industrial architecture. It was a bold move, and world first that would have a durable impact on architecture as a whole. ',
    source: 'https://en.wikipedia.org/wiki/AEG_turbine_factory',
    topicId: "moabit-aeg",
    screenshot: "spon.jpg"
};

DOCUMENTS["aeg-11"] = {
    id: "aeg-11",
    title: 'peter behrens aeg',
    description: 'History and work of architect Peter Behrens connected to the AEG Turbine Factory.',
    source: 'https://adrt.pt/docs/viewtopic.php?59a37b=peter-behrens-aeg',
    topicId: "moabit-aeg",
};

DOCUMENTS["aeg-0"] = {
    id: "aeg-0",
    title: 'Berlin',
    subtitle: 'A ground-breaking industrial classic',
    description: 'Description of AEG Turbine Factory a one of the city\'s most important architectural highlights',
    source: 'http://www.secretcitytravel.com/berlin-feb-2014/berlin-moabit-peter-behrens-factory.shtml',
    topicId: "moabit-aeg",
};

DOCUMENTS["aeg-9"] = {
    id: "aeg-9",
    title: 'AEG Turbine Factory',
    subtitle: 'More than a monument',
    description: 'You will find one of the pivotal moments in architectural history set in stone on Huttenstraße in the district of Moabit.',
    source: 'https://www.visitberlin.de/en/aeg-turbine-factory',
    topicId: "moabit-aeg",
};

DOCUMENTS["aeg-10"] = {
    id: "aeg-10",
    title: 'A Berlin Landmark Keeps on Keeping On',
    subtitle: 'Berliners are old hands at industrial chic. ',
    description: 'They have shown a particular talent for reimagining structures left over from the boom years before World War I. War, division, and aggressive urban planning created plenty of ruins, but the survivors are surprisingly numerous. As any visitor to Berlin will discover, many of these buildings have been converted into a spectacular alternative infrastructure:',
    source: 'https://www.nytimes.com/2010/01/19/arts/19iht-turbine.html',
    topicId: "moabit-aeg",
};

DOCUMENTS["aeg-11"] = {
    id: "aeg-11",
    title: 'AD Classics: Fagus Factory / Walter Gropius + Adolf Meyer',
    subtitle: '',
    description: 'The Fagus Factory is one of the earliest built works of modern architecture, and the first project of Walter Gropius. ',
    source: 'https://www.archdaily.com/612249/ad-classics-fagus-factory-walter-gropius-adolf-meyer',
    topicId: "moabit-aeg",
};

DOCUMENTS["aeg-12"] = {
    id: "aeg-12",
    title: 'When Workers’ Councils Defeated the Far-Right Coup in Germany',
    subtitle: '',
    description: 'On March 13, 1920, twelve million workers struck across Germany to block an attempted military coup. The successful resistance was organized by the workers’ councils — a form of grassroots democracy that allowed the masses to assert their own power. ',
    source: 'https://www.jacobinmag.com/2020/03/novermber-revolution-kapp-putsch-1920',
    topicId: "moabit-aeg",
};

DOCUMENTS["aeg-13"] = {
    id: "aeg-13",
    title: '100 years of Bauhaus: Berlin and beyond ',
    subtitle: 'German holidays',
    description: 'As Germany celebrates the centenary of the influential art movement, we tour the cities where it started, flourished and, ultimately, proved too ‘degenerate’ for the Nazis',
    source: 'https://www.theguardian.com/travel/2019/mar/16/100-years-bauhaus-germany-berlin-weimar-dessau',
    topicId: "moabit-aeg",
};



// Available segments
export const SEGMENTS: {[id: string]: ISegment; } = {};

SEGMENTS["west-harbour-1"] = {
        id: "west-harbour-1",
        documentId: "west-harbour", // document id must match with id in documents
        content: "Initial planning for the construction of a large port in Berlin started",
};

SEGMENTS["west-harbour-2"] = {
        id: "west-harbour-2",
        documentId: "west-harbour",
        content: "West Harbour is a inland harbor in Berlin's Moabit district",
};

SEGMENTS["deutsche-einheit-1"] = {
        id: "deutsche-einheit-1",
        documentId: "deutsche-einheit",
        content: "Der Westhafen ist Bestandteil des aus ökologischen Gründen umstrittenen Verkehrsprojekts Deutsche Einheit Nr. 17",
};

SEGMENTS["deutsche-einheit-2"] = {
        id: "deutsche-einheit-2",
        documentId: "deutsche-einheit",
        content: "Mit dem Ausbau der Wasserstraßenverbindung Hannover–Magdeburg–Berlin können Binnenschiffe mit bis zu 2000 Tonnen",
};

SEGMENTS["deutsche-einheit-3"] = {
        id: "deutsche-einheit-3",
        documentId: "deutsche-einheit",
        content: "und mit einer Abladetiefe von bis zu 2,80 Meter den Westhafen erreichen.",
};

SEGMENTS["aeg-4-0"] = {
        id: "aeg-4-0",
        documentId: "aeg-4",
        content: "The turbine hall was built in 1909 under Peter Behrens as lead architect and engineer Karl Bernhard at the corner of Huttenstraße and Berlichingenstraße streets in Berlin-Moabit.",
};

SEGMENTS["aeg-4-1"] = {
        id: "aeg-4-1",
        documentId: "aeg-4",
        content: "The architect Peter Behrens was commissioned with the construction of the new building. More than an architect, Behrens was employed from 1907 by AEG as an artistic consultant[1] and designed the company logo, and other company graphics for the building.",
};

SEGMENTS["aeg-4-2"] = {
        id: "aeg-4-2",
        documentId: "aeg-4",
        content: "The turbine hall was built in 1909 under Peter Behrens as lead architect and engineer Karl Bernhard[1] at the corner of Huttenstraße and Berlichingenstraße streets in Berlin-Moabit.",
};

SEGMENTS["aeg-4-3"] = {
        id: "aeg-4-3",
        documentId: "aeg-4",
        content: "Since 1956 the building has been classified as a protected historical monument, and it underwent a restoration in 1978. ",
};

SEGMENTS["aeg-11-0"] = {
        id: "aeg-11-0",
        documentId: "aeg-11",
        content: "Peter Behrens published “The art in the technology”, in 1907, in which “Perfekt in Form und Function” (Perfect in form and function) has its origins, that is why the AEG company became interested in his services. In the same year, Behrens held the AEG’s artistic consultant post, being in charge of fashioning all the products of the company and at the same time, he designed the corporate image, becoming this way the first “industrial designer of the history.",
};

SEGMENTS["aeg-11-1"] = {
        id: "aeg-11-1",
        documentId: "aeg-11",
        content: "It is the best-known work of architect Peter Behrens.",
};

SEGMENTS["aeg-0-0"] = {
        id: "aeg-0-0",
        documentId: "aeg-0",
        content: "Dominating an entire block of an otherwise unremarkable neighbourhood in Berlin's Moabit district, the impressive AEG Turbine Factory has to count as one of the city's most important architectural highlights.",
};

SEGMENTS["aeg-0-1"] = {
        id: "aeg-0-1",
        documentId: "aeg-0",
        content: "The streamlined yet monumental structure of steel, glass and concrete quickly set the agenda for an entirely new era of industrial design, making it one of the world's most influential buildings.",
};

SEGMENTS["aeg-9-0"] = {
        id: "aeg-9-0",
        documentId: "aeg-9",
        content: "Never before had anyone attempted to develop a dedicated architectural style for factory buildings. Previously, in line with the prevailing artistic taste of the German Empire, architects had designed historicising façades for industrial buildings.",
};

SEGMENTS["aeg-9-1"] = {
    id: "aeg-9-1",
    documentId: "aeg-9",
    content: "But if you first take a look at the façade from the front, you will see that Behrens and AEG did not completely renounce the tastes of the times. The concrete elements on the left and right are reminiscent of an Egyptian temple. Although distinctly weighty, they have no load-bearing function as they are only exterior cladding and purely for show. Contemporaries understood the sacral allusion and referred to it as the “machine cathedral”.",
};


export const STORIES: IStory[] = [
    {
        topics: [
            {
                object: TOPICS[5], // 'moabit-aeg',
                position: {
                    x: 0,
                    y: 0,
                }
            },
        ],
        segments: [
            {
                object: SEGMENTS['aeg-4-0'],
                position: {
                    x: 0,
                    y: 200,
                },
                rank: 1,
            },
            {
                object: SEGMENTS['aeg-4-1'],
                position: {
                    x: 100,
                    y: 400,
                },
                rank: 2,
                relevance: 0.5,
            },
            {
                object: SEGMENTS['aeg-11-0'],
                position: {
                    x: 100,
                    y: 600,
                },
                rank: 3,
                relevance: 0.5,
            },
            {
                object: SEGMENTS['aeg-0-0'],
                position: {
                    x: 0,
                    y: 800,
                },
                rank: 4,
            },
            {
                object: SEGMENTS['aeg-11-1'],
                position: {
                    x: 0,
                    y: 1000,
                },
                rank: 5
            },
            {
                object: SEGMENTS['aeg-0-1'],
                position: {
                    x: 0,
                    y: 1200,
                },
                rank: 6,
            },
            {
                object: SEGMENTS['aeg-9-0'],
                position: {
                    x: 0,
                    y: 1400,
                },
                rank: 7
            },
            {
                object: SEGMENTS['aeg-9-1'],
                position: {
                    x: 0,
                    y: 1600,
                },
                rank: 8,
            },
            {
                object: SEGMENTS['aeg-4-2'],
                position: {
                    x: 100,
                    y: 1800,
                },
                rank: 9
            },
            {
                object: SEGMENTS['aeg-4-3'],
                position: {
                    x: 0,
                    y: 2000,
                },
                rank: 10,
            }
        ],
        documents: [],
        relations: [
            {
                fromId: 'aeg-4-0',
                toId: 'aeg-4-1',
                label: 'BACKGROUND:Peter_Behrens',
            },
            {
                fromId: 'aeg-4-1',
                toId: 'aeg-11-0',
                label: 'CONTINGENCY_CAUSE',
            },
            {
                fromId: 'aeg-11-0',
                toId: 'aeg-0-0',
                label: 'EXPANSION',
            },
            {
                fromId: 'aeg-4-0',
                toId: 'aeg-0-0',
                label: 'EXPANSION',
            },
            {
                fromId: 'aeg-0-0',
                toId: 'aeg-11-1',
                label: 'CONTINGENCY',
            },
            {
                fromId: 'aeg-11-1',
                toId: 'aeg-0-1',
                label: 'CONTINGENCY_CAUSE',
            },
            {
                fromId: 'aeg-0-1',
                toId: 'aeg-9-0',
                label: 'COMPARISON',
            },
            {
                fromId: 'aeg-9-0',
                toId: 'aeg-9-1',
                label: 'EXPANSION',
            },
            {
                fromId: 'aeg-9-1',
                toId: 'aeg-4-2',
                label: 'TEMPORAL_ASYNCHRONOUS_PRECEDENCE',
            },
            {
                fromId: 'aeg-4-2',
                toId: 'aeg-4-3',
                label: 'TEMPORAL_ASYNCHRONOUS_SUCCESSION',
            },
            {
                fromId: 'aeg-9-1',
                toId: 'aeg-4-3',
                label: 'TEMPORAL_ASYNCHRONOUS_PRECEDENCE',
            },
        ]
    }
];

// Hier kommt der ganze finale Text rein, der bei "Export story" angezeigt wird.
export const STORY_EXPORT: string = 'Gropius was the founder of the Bauhaus, the German "School of Building" that embraced elements of art, architecture, graphic design, interior design, industrial design, and typography in its design, development and production (learn more in our infographic here).\n. Gropius was the founder of the Bauhaus, the German "School of Building" that embraced elements of art, architecture, graphic design, interior design, industrial design, and typography in its design, development and production (learn more in our infographic here).\n Gropius was the founder of the Bauhaus, the German "School of Building" that embraced elements of art, architecture, graphic design, interior design, industrial design, and typography in its design, development and production (learn more in our infographic here).\n Gropius was the founder of the Bauhaus, the German "School of Building" that embraced elements of art, architecture, graphic design, interior design, industrial design, and typography in its design, development and production (learn more in our infographic here).\n Gropius was the founder of the Bauhaus, the German "School of Building" that embraced elements of art, architecture, graphic design, interior design, industrial design, and typography in its design, development and production (learn more in our infographic here).\nGropius was the founder of the Bauhaus, the German "School of Building" that embraced elements of art, architecture, graphic design, interior design, industrial design, and typography in its design, development and production (learn more in our infographic here).\n';

// Helper functions
export function getDocumentsByTopicId(
    topicId: string,
): IDocument[] {
    console.log('getDocumentsByTopicId ', topicId);
    return Object.values(DOCUMENTS).filter(document => topicId === document.topicId);
}

export function getSegmentsByDocumentId(
    documentId: string,
): ISegment[] {
    console.log('getSegmentsByDocumentId ', documentId);
    return Object.values(SEGMENTS).filter(segment => documentId === segment.documentId);
}

export function getSegmentsByTopicId(
    topicId: string,
): ISegment[] {
    console.log('getSegmentsByTopicId ', topicId);
    return getDocumentsByTopicId(topicId).flatMap(document => getSegmentsByDocumentId(document.id));
}
