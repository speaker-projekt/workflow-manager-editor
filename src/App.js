import React from 'react';
import './App.scss';

import PageHeader from './components/PageHeader'

import { usePromiseTracker } from "react-promise-tracker";
import Loader from 'react-loader-spinner';
import StoryEditor from "./components/StoryEditor/StoryEditor";

const LoadingIndicator = props => {
    const { promiseInProgress } = usePromiseTracker();

    return (
        promiseInProgress &&
        <div
            style={{
                width: "100%",
                height: "100px",
                display: "flex",
                justifyContent: "center",
                alignItems: "center"
            }}
        >
            <Loader type="ThreeDots" color="#007bff" height={100} width={100} />
        </div>
    );
};


function App() {
  return (

    <div className="App">
        <PageHeader />
        <LoadingIndicator/>
        <div className="container">
            <main>
                <StoryEditor />
            </main>
        </div>
    </div>

  );
}

export default App;
